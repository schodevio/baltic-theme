<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Baltic
 */

get_header(); ?>

	<div id="primary" class="content-area single-post <?php echo baltictheme_content_width_grid(); ?>">
		<main id="main" class="site-main" role="main">

		<?php
		while ( have_posts() ) : the_post();

			get_template_part( 'template-parts/content', get_post_format() );

			the_post_navigation( array (
				'prev_text'  => '<span><i class="fa fa-angle-double-left"></i>'. __( ' Previous post ', 'baltictheme') .'</span>%title',
				'next_text'  => '<span>'. __( 'Next post ', 'baltictheme') .' <i class="fa fa-angle-double-right"></i></span>%title',
			) );

			if ( get_post_format() != 'status' && get_post_format() != 'quote' && get_post_format() != 'link') : ?>
				<div class = "row post-author-section">
					<div class = "col-xs-12 col-sm-2">
						<div class = "gravatar-author-img">
							<?php echo get_avatar( get_the_author_meta('email'), '120' ); ?>
						</div>
					</div>

					<div class = "col-xs-12 col-sm-10">
						<div class = "author-info">
							<span class = "author-action"><?php echo __( 'Written by ', 'baltictheme'); ?></span>
							<h4><?php the_author_link(); ?></h4>
							<p class = "author-text"><?php the_author_meta('description'); ?></p>
							<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>"><?php echo __( 'More posts by ', 'baltictheme'); the_author(); ?> <i class="fa fa-angle-double-right"></i></a>
							<div class = "author-social-links">
								<h6 class = "author-social-prefix"><span><?php echo __( 'Get in touch: ', 'baltictheme'); ?></span></h6>
								
									<?php baltictheme_author_social_links(); ?>

							</div>
						</div>
					</div>
				</div>
			<?php endif; ?>


			

			<?php
			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();