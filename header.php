<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Baltic
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site" <?php if( ! has_nav_menu( 'primary' ) ) echo 'style = "margin-top: 0;"'; ?>>

	<a class="skip-link screen-reader-text" href="#main"><?php esc_html_e( 'Skip to content', 'baltictheme' ); ?></a>

	<?php if ( has_nav_menu( 'primary' ) ) : ?>
	<header id="masthead" class="site-header clear" role="banner">
		
		<div class = "mobile-btn clear">
			<div class = "toggle-btn toggle-menu"><i class = "fa fa-bars"></i> Menu</div>
		</div>
		
		<nav id="site-navigation" class="main-navigation" role="navigation">
			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
		</nav><!-- #site-navigation -->

	</header><!-- #masthead -->
	<?php endif; ?>

	<?php
		$titan = TitanFramework::getInstance( 'baltictheme' );

		$baltictheme_branding_bg_id = $titan->getOption( 'baltictheme_branding_bg' );
		$baltictheme_branding_logo_id = $titan->getOption( 'baltictheme_branding_logo' );

		if ( is_numeric( $baltictheme_branding_bg_id ) ) {
		    $baltictheme_branding_bg_attachment = wp_get_attachment_image_src( $baltictheme_branding_bg_id, 'full' );
		    $baltictheme_branding_bg_URL = $baltictheme_branding_bg_attachment[0];
		}

		if ( is_numeric( $baltictheme_branding_logo_id ) ) {
		    $baltictheme_branding_logo_attachment = wp_get_attachment_image_src( $baltictheme_branding_logo_id, 'full' );
		    $baltictheme_branding_logo_URL = $baltictheme_branding_logo_attachment[0];
		}

		$baltictheme_branding_bg_color = $titan->getOption( 'baltictheme_branding_color' );
	?>

	<section class="site-branding">
		<div class = "container">
			<?php
			if( isset($baltictheme_branding_logo_URL) ) : ?>
				<div class = "site-logo">
					<!-- screen reader title -->
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src='<?php echo $baltictheme_branding_logo_URL; ?>' /></a>
				</div>
			<?php 
			elseif ( is_front_page() && is_home() ) : ?>
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>

			<?php else : ?>
				<h4 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h4>
			<?php
			endif;

			$description = get_bloginfo( 'description', 'display' );
			if ( $description || is_customize_preview() ) : ?>
				<p class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
			<?php endif; ?>

		</div>
	</section><!-- .site-branding -->

	<div id="content" class="site-content">
		<div class = "container">