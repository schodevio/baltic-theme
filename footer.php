<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Baltic
 */

?>
		</div> <!-- .container -->
	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		
		<?php
		$titan = TitanFramework::getInstance( 'baltictheme' );
		$bottom_sidebar_visibility = $titan->getOption( 'baltictheme-bottom-sidebar' );
		$bottom_sidebar_cols = $titan->getOption( 'bottom-sidebar-columns' );

		if($bottom_sidebar_visibility == 1 && is_active_sidebar( 'sidebar-2' )) : ?>

		<div class = "footer-container">
			<div class = "container">
				<aside id="tetriary" class="widget-area masonry-grid row" role="complementary">
					<div class = "masonry-grid-sizer col-sm-12 <?php echo baltictheme_bottom_widget_size(); ?>"></div>
					<?php dynamic_sidebar( 'sidebar-2' ); ?>
				</aside><!-- #tetriary -->
			</div>
		</div>
				
		<?php
		endif; ?>
	
		<div class = "footer-bottom">
			<div class = "container">
				<div class = "row">
					<?php if ( has_nav_menu( 'social' ) ) : ?>
					<nav class="social-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Footer Social Links Menu', 'baltictheme' ); ?>">
						<?php
							wp_nav_menu( array(
								'theme_location' => 'social',
								'menu_class'     => 'social-links-menu',
								'depth'          => 1,
								'link_before'    => '<span>',
								'link_after'     => '</span>',
							) );
						?>
					</nav><!-- .social-navigation -->
					<?php endif; ?>
				</div>

				<div class = "row">
					<p class = "copy">Copyright (C) <?php echo date('Y'); ?> <?php bloginfo('name'); ?>. <?php echo __('All Rights Reserved.', 'baltictheme'); ?></p>
				
				</div>
			</div>
		</div>

	</footer><!-- #colophon -->

	<a href = "#page" class = "scroll-to-top" title = "<?php echo __('Scroll to top', 'baltictheme'); ?>"><i class="fa fa-arrow-up"></i></a>
</div><!-- #page -->

<?php wp_footer(); ?>

<script type="text/javascript">

	// Customize bottom sidebar
	$( ".widget.masonry-grid-item" ).addClass( "<?php echo baltictheme_bottom_widget_size(); ?>" );

	// Fix embed-container padding for twitter
	$(".twitter-tweet").parents('.embed-container').removeClass("embed-container");

</script>


</body>
</html>
