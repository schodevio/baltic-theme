<?php
/**
 * Template part for displaying results in search pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Baltic
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class = "row">
		<div class = "col-xs-12 col-sm-12 col-md-3">

			<?php
			if ( 'post' === get_post_type() ) : ?>
			<div class="entry-meta">
				<?php baltictheme_posted_on(); ?>
				
			</div><!-- .entry-meta -->
			<?php
			endif; ?>

		</div>

		<div class = "col-xs-12 col-sm-12 col-md-9">

			<!-- post thumbnail -->
			<?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
				<div class = "post-thumbnail">
				<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
					<?php 
					the_post_thumbnail(); // Declare pixel size you need inside the array ?>
				</a>
				</div>
			<?php endif; ?>
			<!-- /post thumbnail -->

			<header class="entry-header">
				<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
			</header><!-- .entry-header -->

			<div class="entry-summary">
				<?php the_excerpt(); ?>
			</div><!-- .entry-summary -->

			<footer class="entry-footer">
				<?php baltictheme_entry_tags(); ?>
			</footer><!-- .entry-footer -->
		</div>
	</div>
	

	
</article><!-- #post-## -->