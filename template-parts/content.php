<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Baltic
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<!-- post thumbnail -->
	<?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
	<div class = "post-thumbnail">
		<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
			<?php the_post_thumbnail(); ?>
		</a>
	</div>
	<?php endif; ?>
	<!-- /post thumbnail -->

	<div class = "post-container clear">
		<div class = "post-date">
			<?php echo baltictheme_date_format(); ?>
		</div>

		<div class = "post-content">
			<header class="entry-header">
				<?php
				if ( is_single() ) {
					the_title( '<h1 class="entry-title">', '</h1>' );
				} else {
					the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
				} ?>
			</header><!-- .entry-header -->

			<ul class = "entry-meta">
				<li class = "entry-author"><?php echo baltictheme_author_info(); ?></li>
				<li class = "entry-comments"><i class="fa fa-comments"></i> <?php echo baltictheme_post_comments(); ?></li>
				<li class = "entry-categories"><i class="fa fa-folder-open"></i> <?php baltictheme_post_categories(); ?></li>
				<li class = "entry-permalink"><i class="fa fa-link"></i> <?php echo baltictheme_post_permalink(); ?></li>
				<li class = "edit-link"><?php baltictheme_post_edit_link(); ?></li>
			</ul>

			<div class="entry-content">
				<?php
					the_content( sprintf(
						/* translators: %s: Name of current post. */
						wp_kses( __( 'Read more %s &rsaquo;&rsaquo;', 'baltictheme' ), array( 'span' => array( 'class' => array() ) ) ),
						the_title( '<span class="screen-reader-text">"', '"</span>', false )
					) );

					wp_link_pages( array(
						'before'      => '<div class="page-links"><div class="page-links-title">' . __( 'Pages:', 'baltictheme' ) . '</div>',
						'after'       => '</div>',
						'link_before' => '<span>',
						'link_after'  => '</span>',
						'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'baltictheme' ) . ' </span>%',
						'separator'   => '<span class="screen-reader-text">, </span>',
					) );

				?>
			</div><!-- .entry-content -->

			<div class = "entry-footer">
				<?php
					if ( is_single() ) {
						baltictheme_entry_tags();
					}
				?>
			</div>
		</div>
	</div>

</article><!-- #post-## -->
