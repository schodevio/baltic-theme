<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Baltic
 */

get_header(); ?>

	<section id="primary" class="content-area search-area <?php echo baltictheme_content_width_grid(); ?>">
		<main id="main" class="site-main" role="main">

		<?php
		if ( have_posts() ) : ?>

			<header class="page-header">
				<h1 class="page-title"><?php printf( esc_html__( 'Search results for: %s', 'baltictheme' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
			</header><!-- .page-header -->

			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();

				/**
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */

				if ( $post->post_type == 'page' ) {
					get_template_part( 'template-parts/content', 'page' );
				} else {
					get_template_part( 'template-parts/content', get_post_format() );
				}

			endwhile;

			the_posts_pagination( array(
				'prev_text'          => __( 'Previous page', 'baltictheme' ),
				'next_text'          => __( 'Next page', 'baltictheme' ),
				'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'baltictheme' ) . ' </span>',
			) ); 

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>

		</main><!-- #main -->
	</section><!-- #primary -->

<?php
get_sidebar();
get_footer();
