<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Baltic
 */

$titan = TitanFramework::getInstance( 'baltictheme' );
$right_sidebar_visibility = $titan->getOption( 'baltictheme-right-sidebar' );

if($right_sidebar_visibility == 1) : ?>
	
	<?php
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		return;
	}
	?>

	<aside id="secondary" class="widget-area col-sm-12 col-md-3" role="complementary">
		<?php dynamic_sidebar( 'sidebar-1' ); ?>
	</aside><!-- #secondary -->

<?php
endif; ?>
