// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());

// Place any jQuery/helper plugins in here.

// TOGGLE MENU


$(document).ready(function() {
    $( ".toggle-menu" ).click(function() {
        $( ".main-navigation" ).slideToggle();
    });
});

$(document).ready(function() {
    $( ".search-btn" ).click(function() {
        $( ".site-header .search-form" ).slideToggle();
    });
});


// MASONRY
$(document).ready(function() {
    $('.masonry-grid').masonry({
      itemSelector: '.masonry-grid-item', // use a separate class for itemSelector, other than .col-
      columnWidth: '.masonry-grid-sizer',
      percentPosition: true
    });
});


// SMOOTH SCROLLING  (TO TOP)
$(document).scroll(function() {
  var y = $(this).scrollTop();
  if (y > 400) {
    $('.scroll-to-top').fadeIn("slow");
  } else {
    $('.scroll-to-top').fadeOut("fast");
  }
});

$(document).ready(function() {
    $('a[href*=#]:not([href=#])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top-110
                }, 750);
                return false;
            }
        }
    });
});




