<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Baltic
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<section class="error-404 not-found">
				<header class="page-header">
					<h1 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'baltictheme' ); ?></h1>
				</header><!-- .page-header -->

				<div class="page-content row">
					<div class = "col-sm-12 col-md-4">
						<p class = "message-404">404</p>
					</div>

					<div class = "col-sm-12 col-md-4">
						<?php the_widget('WP_Widget_Pages', 'sortby=post_modified', 'before_title=<h3>&after_title=</h3>'); ?> 
					</div>

					<div class = "col-sm-12 col-md-4">
						<p class = "error-info"><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links near or a search?', 'baltictheme' ); ?></p>
						<?php
							get_search_form();
						?>
					</div>

				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
