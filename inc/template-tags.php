<?php
/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Baltic
 */


if ( ! function_exists( 'baltictheme_date_format' ) ) :
/**
 * Prints HTML with date.
 */
function baltictheme_date_format() {
	$date_format = '<time class="published" datetime="%1$s"><span class = "day">%2$s</span><span class = "month">%3$s</span></time>';

	$date_format = sprintf( $date_format,
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date( 'd') ),
		esc_html( get_the_date( 'M Y') )
	);

	return $date_format;
}
endif;


if ( ! function_exists( 'baltictheme_author_info' ) ) :
/**
 * Prints HTML with author info.
 */
function baltictheme_author_info() {
	$post_author = sprintf(
		esc_html_x( '%s', 'post author', 'baltictheme' ),
		'<i class="fa fa-user"></i> <a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a>'
	);

	return $post_author;
}
endif;


if ( ! function_exists( 'baltictheme_post_categories' ) ) :
/**
 * Prints HTML with post categories.
 */
function baltictheme_post_categories() {
	$categories_list = get_the_category_list( esc_html__( ', ', 'baltictheme' ) );
	if ( $categories_list && baltictheme_categorized_blog() ) {
		printf( esc_html__( 'Posted in: %1$s', 'baltictheme' ), $categories_list ); // WPCS: XSS OK.
	}
}
endif;


if ( ! function_exists( 'baltictheme_post_comments' ) ) :
/**
 * Prints HTML with post comments.
 */
function baltictheme_post_comments() {
	if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
		$comments_output = '<span class="comments-link"> ';
		$comments_output .= comments_popup_link( esc_html__( 'Leave a comment', 'baltictheme' ), esc_html__( '1 Comment', 'baltictheme' ), esc_html__( '% Comments', 'baltictheme' ) );
		$comments_output .= '</span>';

		return $comments_output;
	} 	
}
endif;


if ( ! function_exists( 'baltictheme_post_permalink' ) ) :
/**
 * Prints HTML with post permalink.
 */
function baltictheme_post_permalink() {
	$post_permalink = sprintf(
		esc_html_x( '%s', 'post permalink', 'baltictheme' ),
		'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . __('Permalink', 'baltictheme') . '</a>'
	);

	return $post_permalink;
}
endif;


if ( ! function_exists( 'baltictheme_post_edit_link' ) ) :
/**
 * Prints HTML with post edit link.
 */
function baltictheme_post_edit_link() {
	edit_post_link(
		sprintf(
			/* translators: %s: Name of current post */
			esc_html__( 'Edit %s', 'baltictheme' ),
			the_title( '<span class="screen-reader-text">"', '"</span>', false )
		),
		'<span><i class="fa fa-scissors"></i> ',
		'</span>'
	);
}
endif;


/**
 * Returns true if a blog has more than 1 category.
 *
 * @return bool
 */
function baltictheme_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'baltictheme_categories' ) ) ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'fields'     => 'ids',
			'hide_empty' => 1,
			// We only need to know if there is more than one category.
			'number'     => 2,
		) );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'baltictheme_categories', $all_the_cool_cats );
	}

	if ( $all_the_cool_cats >= 1 ) {
		// This blog has more than 1 category so baltictheme_categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so baltictheme_categorized_blog should return false.
		return false;
	}
}

/**
 * Flush out the transients used in baltictheme_categorized_blog.
 */
function baltictheme_category_transient_flusher() {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	// Like, beat it. Dig?
	delete_transient( 'baltictheme_categories' );
}
add_action( 'edit_category', 'baltictheme_category_transient_flusher' );
add_action( 'save_post',     'baltictheme_category_transient_flusher' );


if ( ! function_exists( 'baltictheme_entry_tags' ) ) :
/**
 * Prints HTML with tags.
 */
function baltictheme_entry_tags() {

	$tags_list = get_the_tag_list( '', esc_html__( ', ', 'baltictheme' ) );
	if ( $tags_list ) {
		printf( '<span class="tags-links"><i class="fa fa-tags"></i> ' . esc_html__( 'Tagged: %1$s', 'baltictheme' ) . '</span>', $tags_list ); // WPCS: XSS OK.
	}
	
}
endif;




function baltictheme_author_social_links() {
	echo '<ul class = "social-links">';

	$rss_url = get_the_author_meta( 'rss_url' );
	if ( $rss_url && $rss_url != '' ) {
		echo '<li class="rss"><a href="' . esc_url($rss_url) . '"><span>RSS</span></i></a></li>';
	}

	$facebook_profile = get_the_author_meta( 'facebook_profile' );
	if ( $facebook_profile && $facebook_profile != '' ) {
		echo '<li class="facebook"><a href="' . esc_url($facebook_profile) . '"><span>Facebook</span></a></li>';
	}

	$twitter_profile = get_the_author_meta( 'twitter_profile' );
	if ( $twitter_profile && $twitter_profile != '' ) {
		echo '<li class="twitter"><a href="' . esc_url($twitter_profile) . '"><span>Twitter</span></a></li>';
	}
													
	$google_profile = get_the_author_meta( 'google_profile' );
	if ( $google_profile && $google_profile != '' ) {
		echo '<li class="google"><a href="' . esc_url($google_profile) . '" rel="author"><span>Google Plus</span></a></li>';
	}
													
	$linkedin_profile = get_the_author_meta( 'linkedin_profile' );
	if ( $linkedin_profile && $linkedin_profile != '' ) {
        echo '<li class="linkedin"><a href="' . esc_url($linkedin_profile) . '"><span>LinkedIn</span></a></li>';
	}

	$dribbble_profile = get_the_author_meta( 'dribbble_profile' );
	if ( $dribbble_profile && $dribbble_profile != '' ) {
        echo '<li class="dribbble"><a href="' . esc_url($dribbble_profile) . '"><span>Dribbble</span></a></li>';
	}

	$flickr_profile = get_the_author_meta( 'flickr_profile' );
	if ( $flickr_profile && $flickr_profile != '' ) {
        echo '<li class="flickr"><a href="' . esc_url($flickr_profile) . '"><span>Flickr</span></a></li>';
	}

	$github_profile = get_the_author_meta( 'github_profile' );
	if ( $github_profile && $github_profile != '' ) {
        echo '<li class="github"><a href="' . esc_url($github_profile) . '"><span>Github</span></a></li>';
	}

	$instagram_profile = get_the_author_meta( 'instagram_profile' );
	if ( $instagram_profile && $instagram_profile != '' ) {
        echo '<li class="instagram"><a href="' . esc_url($instagram_profile) . '"><span>Instagram</span></a></li>';
	}

	$pinterest_profile = get_the_author_meta( 'pinterest_profile' );
	if ( $pinterest_profile && $pinterest_profile != '' ) {
        echo '<li class="pinterest"><a href="' . esc_url($pinterest_profile) . '"><span>Pinterest</span></a></li>';
	}

	$youtube_profile = get_the_author_meta( 'youtube_profile' );
	if ( $youtube_profile && $youtube_profile != '' ) {
        echo '<li class="youtube"><a href="' . esc_url($youtube_profile) . '"><span>YouTube</span></a></li>';
	}

	echo '</ul>';
}

