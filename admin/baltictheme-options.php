<?php
 

/**
 * Creating theme customizer options via Titan Framework.
 *
 */
 
add_action( 'tf_create_options', 'baltictheme_customizer_options' );
 
function baltictheme_customizer_options() {

	$titan = TitanFramework::getInstance( 'baltictheme' );

	$baltictheme_branding_panel = $titan->createThemeCustomizerSection( array(
	    'name' => __('Site Branding', 'baltictheme'),
	    'panel' => __('Baltic Theme Options', 'baltictheme')
	) );

	$baltictheme_branding_panel->createOption( array(
		'id'   => 'baltictheme_branding_height', 
        'type' => 'number', 
	    'name' => __('Height of site branding section', 'baltictheme'),
	    'desc' => __('Set hight of site branding section (min: 200px, max:550px).', 'baltictheme'),
	    'default' => '450',
        'min' => '200',
    	'max' => '550', 
	    'livepreview' => '$(".site-branding").css("height", value);',
	    'css'  => '.site-branding { height: valuepx;}'
	) );

	$baltictheme_branding_panel->createOption( array(
        'id'   => 'baltictheme_branding_logo', 
        'type' => 'upload', 
        'name' => __('Site logo', 'baltictheme'), 
        'desc' => __('Upload out own logo image.', 'baltictheme'),
    ) );

    $baltictheme_branding_panel->createOption( array(
        'id'      => 'baltictheme_branding_color', 
        'type'    => 'color', 
        'name'    => __('Set site branding background color or ...', 'baltictheme'), 
        'desc'    => __('Choose background color for site branding section.', 'baltictheme'), 
        'default' => '#dedede', 
        'css'  => '.site-branding { background-color: value; }' 
    ) );

    $baltictheme_branding_panel->createOption( array(
        'id'   => 'baltictheme_branding_bg', 
        'type' => 'upload', 
        'name' => __('... site branding background image', 'baltictheme'), 
        'desc' => __('Upload your own background image for site branding section.', 'baltictheme'),
        'css'  => '.site-branding { background-image: value; }'
    ) );




    $baltictheme_sidebars_panel = $titan->createThemeCustomizerSection( array(
	    'name' => __('Sidebars', 'baltictheme'),
	    'panel' => __('Baltic Theme Options', 'baltictheme')
	) );

	$baltictheme_sidebars_panel->createOption( array(
        'id'       => 'baltictheme-right-sidebar', 
        'type'     => 'enable', 
        'name'     => __('Right sidebar', 'baltictheme'), 
        'desc'     => __('Show/Hide right sidebar.', 'baltictheme'), 
        'default'  => true,
        'enabled'  => __('Show', 'baltictheme'), 
        'disabled' => __('Hide', 'baltictheme') 
    ) );

    $baltictheme_sidebars_panel->createOption( array(
        'id'       => 'baltictheme-bottom-sidebar',
        'type'     => 'enable', 
        'name'     => __('Bottom sidebar', 'baltictheme'), 
        'desc'     => __('Show/Hide bottom sidebar.', 'baltictheme'),
        'default'  => true, 
        'enabled'  => __('Show', 'baltictheme'), 
        'disabled' => __('Hide', 'baltictheme') 
    ) );

    $baltictheme_sidebars_panel->createOption( array(
        'id'      => 'bottom-sidebar-columns', 
        'type'    => 'radio', 
        'name'    => __('Number of columns in bottom sidebar', 'baltictheme'), 
        'desc'    => __('Put your widgets in 2, 3 or 4 columns.', 'baltictheme'),  
        'default' => 'cols4',
        'options' => array(
            'cols2' => __('2 columns', 'baltictheme'),
            'cols3' => __('3 columns', 'baltictheme'),
            'cols4' => __('4 columns', 'baltictheme')
        )
    ) );



    $baltictheme_colors_panel = $titan->createThemeCustomizerSection( array(
	    'name' => __('Color Palette', 'baltictheme'),
	    'panel' => __('Baltic Theme Options', 'baltictheme')
	) );

    $baltictheme_colors_panel->createOption( array(
        'id'      => 'baltictheme_primary_text_color', 
        'type'    => 'color', 
        'name'    => __('Primary Text Color', 'baltictheme'),
        'default' => '#111',
        'css'     => '
	        	body,
				button,
				input,
				select,
				textarea {
				    color: value;
				}

				kbd {
					background: value;
				}	

				a,
				a:visited {
					color: value;
				}

				.site-header .toggle-btn {
				    color: value;
				}

				.main-navigation .menu > .menu-item-has-children > a:after,
				.main-navigation .sub-menu > .menu-item-has-children > a:after {
				    color: value;
				}

				.widget-area .widget_nav_menu .sub-menu li a:before,
				.widget-area .widget_archive select,
				.widget-area .widget_categories select {
				    color: value;
				}

				.content-area .pagination .nav-links .dots,
				.content-area .pagination .nav-links .dots:hover,
				.content-area .pagination .nav-links .current,
				.content-area .pagination .nav-links .current:hover {
				    color: value;
				}

				.post-container .post-date .month {
				    color: value;
				}

				.scroll-to-top {
				    color: value;
				}
        '
    ) );

    $baltictheme_colors_panel->createOption( array(
        'id'      => 'baltictheme_secondary_text_color', 
        'type'    => 'color', 
        'name'    => __('Secondary Text Color', 'baltictheme'),
        'default' => '#ccc',
        'css'     => '
        		hr {
				    background-color: value;
				}

				input[type="text"],
				input[type="email"],
				input[type="url"],
				input[type="password"],
				input[type="search"],
				input[type="search"],
				input[type="number"],
				input[type="tel"],
				input[type="range"],
				input[type="date"],
				input[type="month"],
				input[type="week"],
				input[type="time"],
				input[type="datetime"],
				input[type="datetime-local"],
				input[type="color"],
				textarea,
				button,
				input[type="button"],
				input[type="reset"],
				input[type="submit"],
				select {
				    border-color: value;
				}

				@media only screen and (max-width: 782px) {
				    .main-navigation .menu .menu-item a,
				    .main-navigation .sub-menu .menu-item a {
				        border-top: 1px solid value;
				    }
				}

				.widget-area .widget_nav_menu .menu {
				    border-color: value;
				}

				#secondary.widget-area .widget_archive ul li,
				#secondary.widget-area .widget_categories ul li,
				#secondary.widget-area .widget_meta ul li,
				#secondary.widget-area .widget_pages ul li,
				#secondary.widget-area .widget_recent_comments ul li,
				#secondary.widget-area .widget_recent_entries ul li {
				    border-bottom: 1px dashed value;
				}

				.widget-area .widget_archive select,
				.widget-area .widget_categories select {
				    border-color: value;
				}

				.widget-area .widget_calendar table caption {
				    color: value;
				}

				.error-404 .page-header,
				.error-404 .widget_pages ul li {
					border-bottom: 1px dashed value;
				}

				.error-404 .page-content .message-404 {
					color: value;
				}

				.archive-content .page-header,
				.search-area .page-header {
					border-bottom: 1px dashed value;
				}

				.content-area .pagination .nav-links .page-numbers {
				    border-color: value;
				}

				.post .entry-content .page-links > a,
				.page .entry-content .page-links > a {
				    border-color: value;
				}

				.content-area .post {
				    border-bottom: 1px dashed value;
				}

				.post .entry-footer .tags-links,
				.post .entry-footer .tags-links a {
				    color: value;
				}

				.post table tbody tr,
				.comment .comment-content table tbody tr {
				    border-bottom: 1px solid value;
				}

				.post.format-link .entry-content a {
					border-color: value;
				}

				.comment-list .comment-body {
				    border-bottom: 1px dashed value;
				}

				.comment-list .comment-body .comment-metadata > a {
				    color: value;
				}

				.comment-list .comment-body .reply a {
				    border-color: value;
				}

				.scroll-to-top {
				    border-color: value;
				}
        '
    ) );

    $baltictheme_colors_panel->createOption( array(
        'id'      => 'baltictheme_first_color', 
        'type'    => 'color', 
        'name'    => __('First Theme Color', 'baltictheme'),
        'default' => '#fac03b',
        'css'     => '
        		blockquote,
				q {
				    border-color: value;
				}

				input[type="text"]:focus,
				input[type="email"]:focus,
				input[type="url"]:focus,
				input[type="password"]:focus,
				input[type="search"]:focus,
				input[type="number"]:focus,
				input[type="tel"]:focus,
				input[type="range"]:focus,
				input[type="date"]:focus,
				input[type="month"]:focus,
				input[type="week"]:focus,
				input[type="time"]:focus,
				input[type="datetime"]:focus,
				input[type="datetime-local"]:focus,
				input[type="color"]:focus,
				textarea:focus {
				    box-shadow: 0 0 5px value;
				    border-color: value;
				}

				a:hover,
				a:focus,
				a:active {
				    color: value;
				}

				.main-navigation .menu > .menu-item-has-children > a:hover:after,
				.main-navigation .sub-menu > .menu-item-has-children > a:hover:after {
				    color: value;
				}

				.main-navigation .menu .menu-item a:hover {
				    color: value;
				}

				.site-footer .social-navigation a:hover {
				    color: value;
				}

				.widget-area .widget_nav_menu .sub-menu li a:hover:before {
					color: value;
				}

				.widget-area .widget_archive ul li a:hover:before,
				.widget-area .widget_categories ul li a:hover:before,
				.widget-area .widget_meta ul li a:hover:before,
				.widget-area .widget_pages ul a:hover:before,
				.widget-area .widget_recent_entries ul li a:hover:before,
				.widget-area .widget_archive ul a:hover,
				.widget-area .widget_categories ul a:hover,
				.widget-area .widget_meta ul a:hover,
				.widget-area .widget_pages ul a:hover {
				    color: value;
				}

				.widget-area .widget_calendar table #today {
				    background: value;
				}

				.widget-area .widget_tag_cloud .tagcloud a:hover {
				    background: value;
				}

				.error-404 .widget_pages ul a:hover,
				.error-404 .widget_pages ul a:hover:before {
					color: value;
				}

				.content-area .pagination .nav-links .current,
				.content-area .pagination .nav-links .current:hover {
				    background: value;
				    border-color: value;
				}

				.post .entry-content .page-links > span,
				.page .entry-content .page-links > span {
				    bordercolor: value;
				    background: value;
				}

				.post-container .post-date .month {
				    background: value;
				}

				.post .entry-title em,
				.page .entry-title em {
				    color: value;
				}

				.post.sticky .entry-header .entry-title a:before {
				    color: value;
				}

				.post .entry-content .more-link:hover {
				    color: value;
				}

				.post.format-quote .entry-content .quote-icon,
				.post.format-link .entry-content .link-icon {
					background: value;
				}

				.comments-area .comments-title {
				    background: value;
				}

				.comment-list .comment-metadata .comment-edit-link {
				    color: value;
				}
        '
    ) );

    $baltictheme_colors_panel->createOption( array(
        'id'      => 'baltictheme_second_color', 
        'type'    => 'color', 
        'name'    => __('Second Theme Color', 'baltictheme'),
        'default' => '#2c3e50',
        'css'     => '
				.widget-area .widget_tag_cloud .tagcloud a {
				    background: value;
				}
        '
    ) );

    $baltictheme_colors_panel->createOption( array(
        'id'      => 'baltictheme_third_color', 
        'type'    => 'color', 
        'name'    => __('Third Theme Color', 'baltictheme'),
        'default' => '#3498db',
        'css'     => '
	        	code {
					color: value;
				}

				button:hover,
				input[type="button"]:hover,
				input[type="reset"]:hover,
				input[type="submit"]:hover {
				    background: value;
				    border-color: value;
				}

				.site-header .toggle-btn:hover {
				    background: value;
				}

				.content-area .pagination .nav-links .page-numbers:hover {
				    background: value;
				    border-color: value;
				}

				.post .entry-content .page-links > a:hover,
				.page .entry-content .page-links > a:hover {
				    background: value;
				    border-color: value;
				}

				.post-container .post-date .day {
				    background: value;
				}

				.post .entry-footer .tags-links a:hover {
				    color: value;
				}

				.post table thead,
				.comment .comment-content table thead {
				    background: value;
				}

				.post figure figcaption {
					color: value;
				}

				.comment-list .comment-body .reply a:hover {
				    background: value;
				    border-color: value;
				}

				.scroll-to-top:hover,
				.scroll-to-top:active,
				.scroll-to-top:focus {
				    background: value;
				    border-color: value;
				}
        '
    ) );

    $baltictheme_colors_panel->createOption( array(
        'id'      => 'baltictheme_fourth_color', 
        'type'    => 'color', 
        'name'    => __('Fourth Theme Color', 'baltictheme'),
        'default' => '#ecf0f1',
        'css'     => '
        		pre,
        		code,
        		.site-footer .footer-container {
				    background: value;
				}
        '
    ) );
}



if ( ! function_exists( 'baltictheme_content_width_grid' ) ) :
/**
 * Set full width to content if there is not right sidebar
 */
function baltictheme_content_width_grid() {
	$titan = TitanFramework::getInstance( 'baltictheme' );
	$right_sidebar_visibility = $titan->getOption( 'baltictheme-right-sidebar' );

	if($right_sidebar_visibility == 1) {
		$content_style = 'col-sm-12 col-md-9';
	} else {
		$content_style = 'col-md-12';
	}

	return $content_style;
}
endif;


if ( ! function_exists( 'baltictheme_bottom_widget_size' ) ) :
/**
 * Set full width to content if there is not right sidebar
 */
function baltictheme_bottom_widget_size() {
	$titan = TitanFramework::getInstance( 'baltictheme' );
	$bottom_sidebar_cols = $titan->getOption( 'bottom-sidebar-columns' );

	switch( $bottom_sidebar_cols ) {
		case 'cols2':
			$bottom_widgets_size = 'col-md-6';
			break;
		case 'cols3':
			$bottom_widgets_size = 'col-md-4';
			break;
		case 'cols4':
			$bottom_widgets_size = 'col-md-3';
			break;
	}

	return $bottom_widgets_size;
}
endif;

/**
 * Remove unnecessary Customizer sections
 */
function baltictheme_remove_sections(){
    global $wp_customize;

    $wp_customize->remove_section('colors');
    $wp_customize->remove_section('header_image');
}

// Priority 20 so that we remove options only once they've been added
add_action( 'customize_register', 'baltictheme_remove_sections', 20 );
